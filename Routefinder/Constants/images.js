/**
 * Define or change the images
 */

const logo = require('../assets/logo.png');
const intro_one = require('../assets/Vector.png');
const intro_two = require('../assets/Vector1.png');
const intro_three = require('../assets/Vector2.png');
const intro_four = require('../assets/Vector3.png');
const Location = require('../assets/Location.png');

/**
 * export the images object
 */
export default {
  logo,
  intro_one,
  intro_two,
  intro_three,
  intro_four,
  Location,
};
